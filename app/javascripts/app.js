// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

// Import our contract artifacts and turn them into usable abstractions.
import stromsome_artifacts from '../../build/contracts/Stromsome.json'

// Stromsome is our usable abstraction, which we'll use through the code below.
var Stromsome = contract(stromsome_artifacts);
const contract_address = "0xBEB4Dde5247772811e3aF49F6F4a64bD12b70390";

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;

window.App = {
  start: function() {
    var self = this;

    web3.setProvider(new web3.providers.HttpProvider('http://bclabq7wx.westeurope.cloudapp.azure.com:8545'));

    // Bootstrap the Stromsome abstraction for Use.
    Stromsome.setProvider(web3.currentProvider);
    Stromsome.setNetwork("10101010");

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[3];
      console.log("Account: " + account);
      web3.personal.unlockAccount(account, "stromesome", 1000);

      self.refreshBalance();
    });
  },

  setStatus: function(message) {
    var status = document.getElementById("status");
    status.innerHTML = message;
  },

  setResult: function(message) {
    var result = document.getElementById("result");
    result.innerHTML = message;
  },

  refreshBalance: function() {
    var self = this;
    var meta;

    var amount = parseInt(document.getElementById("amount").value);

    Stromsome.at(contract_address).then(function(instance) {
      meta = instance;
      return meta.Wh_rate.call(account, {from: account});
    }).then(function(value) {
      var balance_element = document.getElementById("balance");
      balance_element.innerHTML = value.valueOf();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error; see log.");
    });

    // Stromsome.at(contract_address).then(function(instance) {
    //   meta = instance;
    //   return meta.maxChargeAmount.call(account, {from: account});
    // }).then(function(value) {
    //   var contract_element = document.getElementById("contract");
    //   contract_element.innerHTML = value.valueOf();
    // }).catch(function(e) {
    //   console.log(e);
    //   self.setStatus("Error; see log.");
    // });

    // account balance
    var myAccount = web3.fromWei(web3.eth.getBalance(account), "ether");

    var myAccount_element = document.getElementById("account");
    myAccount_element.innerHTML = myAccount.valueOf();

    // contract balance (max charge amount)
    // 1: wei
    // 10^3: (unspecified)
    // 10^6: (unspecified)
    // 10^9: (unspecified)
    // 10^12: szabo
    // 10^15: finney
    // 10^18: ether
    var contract = web3.eth.getBalance(contract_address) / 1000000000000000000;

    contract = contract - 491 - 86;

    var contract_element = document.getElementById("contract");
    contract_element.innerHTML = contract.valueOf();

  },

  sendAmount: function() {
    var self = this;

    var amount = parseInt(document.getElementById("amount").value);

    this.setStatus("Initiating transaction... (please wait)");

    var meta;
    Stromsome.at(contract_address).then(function(instance) {
      meta = instance;
      return meta.setRate(amount, {from: account});
    }).then(function() {
      self.setStatus("Transaction complete!");
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error setting rate; see log.");
    });
  },

  sendValue: function() {
    var self = this;

    var value = parseInt(document.getElementById("value").value);

    this.setResult("Initiating transaction... (please wait)");

    var meta;
    Stromsome.at(contract_address).then(function(instance) {
      meta = instance;
      return meta.confirmProvided(value, {from: account});
    }).then(function() {
      self.setResult("Transaction complete!");
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setResult("Error setting rate; see log.");
    });
  }

};

window.addEventListener('load', function() {

  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://bclabq7wx.westeurope.cloudapp.azure.com:8545"));
  }

  App.start();
});
