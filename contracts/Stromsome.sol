pragma solidity ^0.4.11;

//TODO: manuell Methoden aufrufen und nachvollziehen. ChargeBMW funktioniert (account wird belastet), die anderen beiden Funktionen nicht.

//TODO: kundeniniziiertes abort implementieren
//TODO: auch ein fully charged vom Auto anlegen
//TODO: im Moment gibt die Ladesaeule immer einen Batch zu viel aus.
//TODO: Infos für verschiedene Kunden in einem Array anlegen
//TODO: Balance zurücküberweisen, wenn nicht genutzt?
//TODO: Status einbauen, dass man die Rate nicht mehr ändern kann, sobald chargeBMW aufgerufen wurde. Erst bei fullyCharged zurücksetzen.

contract Stromsome {
    uint public Wh_rate = 1;
    uint public maxChargeAmount;
    uint public chargedSoFar = 0;
    address public electricityProvider;
    address public bmwVehicle;

    function Stromsome() {
        electricityProvider = msg.sender;
    }

    function setRate(uint rate)
                onlyElectricityProvider
    {
        Wh_rate = rate;
    }

    modifier condition(bool _condition) {
        if (!_condition) throw;
        _;
    }

    modifier onlybmwVehicle() {
        require(msg.sender == bmwVehicle);
        _;
    }

    modifier onlyElectricityProvider() {
        require(msg.sender == electricityProvider);
        _;

    }


    event purchaseConfirmed(uint amount);
    event fullyCharged();

    //I want to charge my BMW with a certain amount
    function chargeBMW()
                               payable
    {
        bmwVehicle = msg.sender;
        maxChargeAmount = msg.value/1000000000000000000 ;
        chargedSoFar = 0;

                //Über payable wird autoamtisch das Geld geblockt
                purchaseConfirmed(maxChargeAmount);
    }


    /// Ladesäule confirms that the amount has been transferred
    function confirmProvided(uint transferred_Wh)
        onlyElectricityProvider
    {
                                               uint currentBatchCost = transferred_Wh  * Wh_rate;

                                               //Don't confirm transaction if it's more than the user wanted to charge and inform that the vehicle is fully charged
                                               chargedSoFar = chargedSoFar + currentBatchCost;

                                               if (chargedSoFar > maxChargeAmount) {
                                                               fullyCharged();
                                                              if (!electricityProvider.send((maxChargeAmount-chargedSoFar+currentBatchCost)*1000000000000000000)) throw; //transfer the remaining money in the contract. maxChargeAmount minus what has been payed so far (=chargedSoFar of last iteration = (chargedSoFar + currentBatchCost))
                                               } else {
                                               //pay the money for the transferred Wh
                if (!electricityProvider.send(currentBatchCost * 1000000000000000000)) throw;
        }

    }
}
